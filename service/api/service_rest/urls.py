from django.urls import path

from .views import (
    api_technicians,
    api_delete_technician,
    api_list_appointments,
    api_show_appointment,
)

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians_list"),
    path("technicians/<int:pk>/", api_delete_technician, name="api_delete_technician"),
    path("technicians/new", api_technicians, name="api_create_technician"),
    path("service/", api_list_appointments, name="api_list_appointments"),
    path("service/<int:pk>/", api_show_appointment, name="api_show_appointment"),
    path("service/new", api_list_appointments, name="api_create_appointment"),
    ]