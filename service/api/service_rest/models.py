from django.db import models

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)

class Technician(models.Model):
    name = models.CharField(max_length = 50)
    employee_number = models.CharField(max_length=5, unique= True)

    def __str__(self):
        return self.name

class ServiceAppointment(models.Model):
    vin = models.CharField(max_length=17)
    owner = models.CharField(max_length=50)
    date_time = models.DateTimeField()
    reason = models.TextField()
    vip_status = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.owner

