from re import L, T
from django.shortcuts import render
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, ServiceAppointment
import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin"
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id",
    ]

class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "owner",
        "date_time",
        "reason",
        "id",
        "vip_status",
        "completed",
    ]
    encoders = {
        "vin": AutomobileVOEncoder()
    } 
    def get_extra_data(self, o):
        return {"vin": o.vin, "technician": o.technician.name}

#list and create technicians 
@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not enter technician"}
            )
            response.status_code = 400
            return response

#delete technician
@require_http_methods(["DELETE"])
def api_delete_technician(request, pk):
    try:
        technician = Technician.objects.get(id=pk)
        technician.delete()
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
    except Technician.DoesNotExist:
        return JsonResponse({"message": "Technician does not exist to be deleted"})

#list and create appointments
@require_http_methods(["GET", "POST"]) 
def api_list_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=ServiceAppointmentEncoder,
            )
        
    else:
        content = json.loads(request.body)
        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician

        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee id"}, status=404
            )
        

        content["vip_status"] = AutomobileVO.objects.filter(vin=content["vin"]).exists()
        appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            appointment, encoder=ServiceAppointmentEncoder, safe=False
        )

#change specific appointment status and delete specific appointment
@require_http_methods(["DELETE", "PUT"])
def api_show_appointment(request, pk):
    if request.method == "DELETE":
        count, _ = ServiceAppointment.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        ServiceAppointment.objects.filter(id=pk).update(**content)
        service = ServiceAppointment.objects.get(id=pk)
        return JsonResponse(
            service,
            encoder=ServiceAppointmentEncoder,
            safe=False
        )
