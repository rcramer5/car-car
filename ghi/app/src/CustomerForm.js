import React from "react";

class CustomerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            address: "",
            phone_number: ""
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const value = event.target.value;
        const key = event.target.name;
        const changeDict = {}
        changeDict[key] = value;
        this.setState(changeDict);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        
        const customerUrl = "http://localhost:8090/api/customers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const customerResponse = await fetch(customerUrl, fetchConfig);
        if (customerResponse.ok){
            const newCustomer = await customerResponse.json()
            console.log(newCustomer)
            const cleared = {
                name: "",
                address: "",
                phone_number: "",
            };
            this.setState(cleared);
        }
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Register a New Customer</h1>
                        <form onSubmit={this.handleSubmit} id="create-customer-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.name} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.address} placeholder="Address" required type="text" id="address" name="address" className="form-control"/>
                                <label htmlFor="address">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.phone_number} placeholder="Phone Number" required type="text" id="phone_number" name="phone_number" className="form-control"/>
                                <label htmlFor="phone_number">Phone Number</label>
                            </div>
                            <button className="btn btn-success">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default CustomerForm;