import React from "react";

class ModelForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            manufacturers: [],
            picture_url: "",
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        const value = event.target.value;
        const key = event.target.name;
        const changeDict = {}
        changeDict[key] = value
        this.setState(changeDict);
    }

    async componentDidMount(){
        const manufacturerUrl = "http://localhost:8100/api/manufacturers"
        const manufacturerResponse = await fetch(manufacturerUrl)

        if (manufacturerResponse.ok){
            const manufacturerData = await manufacturerResponse.json()
            this.setState({manufacturers: manufacturerData.manufacturers})
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        delete data.manufacturers
        
        const modelUrl = "http://localhost:8100/api/models/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };
        const modelResponse = await fetch(modelUrl, fetchConfig);
        if (modelResponse.ok){
            const cleared = {
                name: "",
                manufacturer: "",
                picture_url: "",
            };
            this.setState(cleared);
        }
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a Model</h1>
                        <form onSubmit={this.handleSubmit} id="create-model-form">
                            <div className="mb-3">
                                <select onChange={this.handleChange} value={this.state.manufacturer} required name="manufacturer" id="manufacturer" className="form-control">
                                    <option value="">Select a Manufacturer</option>
                                    {this.state.manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.id} value={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.name} required placeholder="Model Name" type="text" id="name" name="name" className="form-control" />
                                <label htmlFor="name">Model Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.picture_url} required placeholder="Picture URL" type="text" id="picture_url" name="picture_url" className="form-control" />
                                <label htmlFor="picture_url">Picture URL</label>
                            </div>
                            <div className="text-center">
                                <button className="btn btn-success">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default ModelForm;