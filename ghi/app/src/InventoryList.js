import React from "react";

class InventoryList extends React.Component{
    constructor(props){
        super(props)
        this.state = {automobiles: [], sales: [],}
    }

    async componentDidMount() {
        const automobilesUrl = "http://localhost:8100/api/automobiles/";
        const response = await fetch(automobilesUrl);
        if (response.ok){
            const data = await response.json()
            this.setState({automobiles: data.autos})
        }
        const salesUrl = "http://localhost:8090/api/sale_records/";
        const salesResponse = await fetch(salesUrl);
        if (salesResponse.ok){
            const salesData = await salesResponse.json()
            this.setState({sales: salesData.sale_records})
        }
    }

    render() {
        return (
            <div>
                <br>
                </br>
                <h1>Inventory</h1>
                <br></br>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Color</th>
                            <th>Year</th>
                            <th>Model</th>
                            <th>Manufacturer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.automobiles.map(automobile => {
                            let shown = ""
                            let sold_vins = []
                            for (let sale of this.state.sales){
                                sold_vins.push(sale.automobile.vin)
                            }
                            if (sold_vins.includes(automobile.vin)){
                                shown = "d-none"
                            }
                            return (
                            <tr className={shown} key={automobile.href}>
                                <td>{automobile.vin}</td>
                                <td>{automobile.color}</td>
                                <td>{automobile.year}</td>
                                <td>{automobile.model.name}</td>
                                <td>{automobile.model.manufacturer.name}</td>
                            </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default InventoryList;