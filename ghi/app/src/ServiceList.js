import React from "react";
import { useState } from "react-router-dom";

class ServiceList extends React.Component
{
    constructor(props)
    {
        super(props)
        this.state = 
        {
            appointments: []
        }
    }


    //need list of current appointments
    async componentDidMount()
    {
        const url = 'http://localhost:8080/api/service/';
        const response = await fetch(url);

        if (response.ok) 
        {
            const data = await response.json();
            console.log(data)
            this.setState({ appointments: data.appointments });
        }
    
    }
    async handleCancel(id) {
        const cancelUrl = `http://localhost:8080/api/service/${id}/`;
        const fetchConfig = {
          method: "delete",
        };
  
        const response = await fetch(cancelUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
          console.log("deleted");
          let {appointments} = this.state
          appointments = appointments.filter(appointment => appointment.id !== id)
          this.setState({appointments})
        }
      };
  
      async handleFinished(id) {
            const finishedUrl = `http://localhost:8080/api/service/${id}/`;
            const fetchConfig = {
            method: "put",
            body: JSON.stringify({completed: true}),
            headers: {
              'Content-Type': 'application/json',
            },
          };
    
          const response = await fetch(finishedUrl, fetchConfig);
          console.log(response)
          if (response.ok) {
            console.log("updated");
            let {appointments} = this.state
            appointments = appointments.filter(appointment => appointment.completed === false)
            this.setState({appointments})
            //window.location.reload(false)
            }
        
        };
  
    render()
    {
        return (
            <>
            <h1>Scheduled Appointments</h1>
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>VIP</th>
                        <th>Owner</th>
                        <th>VIN</th>
                        <th>Service Date</th>
                        <th>Service Time</th>
                        <th>Tech</th>
                        <th>Reason</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.appointments.map(appointment => 
                        {
                            const date = new Date(appointment.date_time).toLocaleDateString();
                            const time = new Date(appointment.date_time).toLocaleTimeString([], {timeStyle: 'short'});
                           
                            
                            let completed = ''
                            if (appointment.completed === true) 
                            {
                                completed = 'd-none'
                            }
                            return (
                            <tr className={completed} key={appointment.id}>
                              <td>{ appointment.vip_status ? "YES" : "NO" } </td>
                              <td>{ appointment.owner }</td>
                              <td>{ appointment.vin }</td>
                              <td>{ date } </td>
                              <td>{ time } </td>
                              <td>{ appointment.technician }</td>
                              <td>{ appointment.reason }</td>
                              <td><button onClick={()=>this.handleCancel(appointment.id)} to="" className="btn btn-danger">Cancel</button></td>
                              <td><button onClick={()=>this.handleFinished(appointment.id)} to="" className="btn btn-success">Complete</button></td>
                            </tr>
                          );
                        })}
                </tbody>
            </table>
        </>
    )
    }
}

export default ServiceList;