import React from "react"

class SalesList extends React.Component{
    constructor(props) {
        super(props)
        this.state = {sale_records: []}
    }

    async componentDidMount() {
        const salesUrl = "http://localhost:8090/api/sale_records/";
        const salesResponse = await fetch(salesUrl);
        const salesJSON = await salesResponse.json()
        this.setState({sale_records: salesJSON.sale_records})
    }

    render() {
        return (
            <div>
                <br></br>
                <h1>Sales History</h1>
                <br>
                </br>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Sales Person Name</th>
                            <th>Sales Person Employee #</th>
                            <th>Customer Name</th>
                            <th>Automobile VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.sale_records.map(sale => (
                            <tr key={sale.id}>
                                <td>{sale.sales_person.name}</td>
                                <td>{sale.sales_person.employee_number}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.sale_price}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }




}

export default SalesList