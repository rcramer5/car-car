import React from "react";

class AutomobileForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            color: "",
            year: "",
            vin: "",
            models: [],
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        const value = event.target.value;
        const key = event.target.name;
        const changeDict = {}
        changeDict[key] = value
        this.setState(changeDict);
    }

    async componentDidMount(){
        const modelUrl = "http://localhost:8100/api/models"
        const modelResponse = await fetch(modelUrl)

        if (modelResponse.ok){
            const modelData = await modelResponse.json()
            this.setState({models: modelData.models})
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        delete data.models
        
        const automobileUrl = "http://localhost:8100/api/automobiles/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };
        const createResponse = await fetch(automobileUrl, fetchConfig);
        if (createResponse.ok){
            const cleared = {
                color: "",
                year: "",
                vin: "",
                model: "",
            };
            this.setState(cleared);
        }
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add an Automobile</h1>
                        <form onSubmit={this.handleSubmit} id="create-automobile-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.color} required placeholder="Color" type="text" id="color" name="color" className="form-control" />
                                <label htmlFor="name">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.year} required placeholder="Year" type="text" id="year" name="year" className="form-control" />
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.vin} required placeholder="VIN" type="text" id="vin" name="vin" className="form-control" />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} value={this.state.model} required name="model" id="model" className="form-control">
                                    <option value="">Select a Model</option>
                                    {this.state.models.map(model => {
                                        return (
                                            <option key={model.id} value={model.id}>
                                                {model.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="text-center">
                                <button className="btn btn-success">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default AutomobileForm;