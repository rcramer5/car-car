import React from "react";

class SalesPersonForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            employee_number: "",
            sales_people: [],
            employee_numbers: [],
            unique: "unique",
            numberLength:"not valid",
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNumberChange = this.handleNumberChange.bind(this);
    }

    handleNumberChange(event){
        const numberValue = event.target.value
        this.setState({employee_number: numberValue})
        let data = {...this.state}
        if (data.employee_numbers.includes(employee_number.value) === true) {
            this.setState({unique: "not unique"})
        } else{
            this.setState({unique: "unique"})
        }
        if (employee_number.value.length === 5 ){
            this.setState({numberLength: "valid"})
        } else {
            this.setState({numberLength: "not valid"})
        }
    }

    handleChange(event) {
        const value = event.target.value;
        const key = event.target.name;
        const changeDict = {}
        changeDict[key] = value;
        this.setState(changeDict);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.sales_people
        delete data.employee_numbers
        delete data.unique
        delete data.numberLength
        
        const salesPersonUrl = "http://localhost:8090/api/sales_person/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const salesPersonResponse = await fetch(salesPersonUrl, fetchConfig);
        if (salesPersonResponse.ok){
            const cleared = {
                name: "",
                employee_number: "",
                unique: "unique",
                employee_numbers: [],
                numberLength: "not valid"
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8090/api/sales_person/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            this.setState({sales_people: data.sales_people})
            const employee_numbers_list = data.sales_people.map(({employee_number}) => employee_number)
            this.setState({employee_numbers: employee_numbers_list})
        }
    }

    render() {
        let unique = this.state.unique
        let numberLength = this.state.numberLength
        let uniqueValue 
        let lengthValid
        if (this.state.unique == "unique"){
            uniqueValue = <div className="text-success"><p className="font-weight-bold">{this.state.unique}</p></div>
        } else if (this.state.unique == "not unique"){
            uniqueValue = <div className="text-danger"><p className="font-weight-bold">{this.state.unique}</p></div>
        }
        if (this.state.numberLength == "valid"){
            lengthValid = <div className="text-success"><p className="font-weight-bold">{this.state.numberLength}</p></div>
        } else {
            lengthValid = <div className="text-danger"><p className="font-weight-bold">{this.state.numberLength}</p></div>
        }
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Register a New Sales Person</h1>
                        <form onSubmit={this.handleSubmit} id="create-salesperson-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.name} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNumberChange} value={this.state.employee_number} placeholder="Employee Number" required type="text" id="employee_number" name="employee_number" className="form-control"/>
                                <label htmlFor="employee_number">Employee Number</label>
                                <div className="float-left"><p>The current employee number is</p></div>
                                {uniqueValue}
                                <div className="float-left"><p>The current employee number length is</p></div>
                                {lengthValid}
                            </div>
                            <button className="btn btn-success">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default SalesPersonForm;