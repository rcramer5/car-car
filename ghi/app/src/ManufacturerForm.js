import React from "react";

class ManufacturerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: ""
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        const value = event.target.value;
        const key = event.target.name;
        const changeDict = {}
        changeDict[key] = value
        this.setState(changeDict);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        
        const manufacturerUrl = "http://localhost:8100/api/manufacturers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };
        const manufacturerResponse = await fetch(manufacturerUrl, fetchConfig);
        if (manufacturerResponse.ok){
            const cleared = {
                name: "",
            };
            this.setState(cleared);
        }
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a Manufacturer</h1>
                        <form onSubmit={this.handleSubmit} id="create-manufacturer-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.name} required placeholder="Name" type="text" id="name" name="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="text-center">
                                <button className="btn btn-success">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default ManufacturerForm