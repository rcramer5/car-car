import React from "react";

class ManufacturerList extends React.Component{
    constructor(props){
        super(props)
        this.state = {manufacturers: []}
    }

    async componentDidMount() {
        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(manufacturerUrl);
        const data = await response.json()
        this.setState({manufacturers: data.manufacturers})
    }

    render() {
        return (
            <div>
                <br>
                </br>
                <h1>Manufacturers</h1>
                <br></br>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Manufacturer Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.manufacturers.map(manufacturer => (
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ManufacturerList;