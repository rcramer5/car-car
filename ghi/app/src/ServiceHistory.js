import React from 'react';

class ServiceHistory extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        appointments: [],
        search: '',
      };
      this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const newState = {}
        newState[event.target.id] = event.target.value;
        this.setState(newState)
    }
    
    async componentDidMount() {
        const url = 'http://localhost:8080/api/service/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          console.log(data)
          this.setState({ appointments: data.appointments });
        }
      }
      async handleSearch(event) {
        event.preventDefault();
    
        const searchUrl = `http://localhost:8080/api/service/`;
        const fetchConfig = {
          method: "get",
        };

        const response = await fetch(searchUrl, fetchConfig);
        console.log(response)
        }
render(){
    return(
        <>
        <div>
          <br>
          </br>
          <form className="form-inline" onSubmit={this.handleSearch} id="search-vin">
              <div className="input-group mb-3">
                  <input onChange={this.handleChange} value={this.state.search} type="text" placeholder="Enter VIN #" required name="search" id="search" className="form-control" />
                  <label htmlFor="search"></label>
                  <div className="input-group-prepend"></div>
              </div>
          <h1>Completed Appointments</h1>
              <table className="table table-striped table-hover">
                  <thead>
                      <tr>
                          <th>VIP</th>
                          <th>Owner</th>
                          <th>VIN</th>
                          <th>Service Date</th>
                          <th>Service Time</th>
                          <th>Tech</th>
                          <th>Reason</th>

                      </tr>
                  </thead>
                  <tbody>
                      {this.state.appointments.map(appointment => 
                          {
                              const date = new Date(appointment.date_time).toLocaleDateString();
                              const time = new Date(appointment.date_time).toLocaleTimeString([], {timeStyle: 'short'});
                  
                              let vin = ''
                              if (appointment.vin !== this.state.search) 
                              {
                                  vin = 'd-none'
                              }
                              return (
                              <tr className={vin} key={appointment.id}>
                                <td>{ appointment.vip_status ? "YES" : "NO" } </td>
                                <td>{ appointment.owner }</td>
                                <td>{ appointment.vin }</td>
                                <td>{ date } </td>
                                <td>{ time } </td>
                                <td>{ appointment.technician }</td>
                                <td>{ appointment.reason }</td>
                              </tr>
                            );
                          })}
                  </tbody>
              </table>
          </form>
        </div>
        </>
    )}
}
export default ServiceHistory;