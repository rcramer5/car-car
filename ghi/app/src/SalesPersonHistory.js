import React from "react";


class SalesPersonHistory extends React.Component{
    constructor(props){
        super(props)
        this.state = {sale_records: [], sales_people: [], sales_person: "", }
        this.handleChange = this.handleChange.bind(this);
    }

    async componentDidMount() {
        const salesPersonUrl = "http://localhost:8090/api/sales_person/";

        const salesPersonResponse = await fetch(salesPersonUrl)

        if (salesPersonResponse.ok){
            const salesPersonData = await salesPersonResponse.json()
            this.setState({sales_people: salesPersonData.sales_people})
        }
    }
    
    async handleChange(event) {
        const value = event.target.value;
        const key = event.target.name;
        const changeDict = {}
        changeDict[key] = value;
        this.setState(changeDict);
        const saleRecordsUrl = "http://localhost:8090/api/sale_records/";
        const salesResponse = await fetch(saleRecordsUrl);
    
        if (salesResponse.ok){
            const saleRecordsData = await salesResponse.json()
            this.setState({sale_records: saleRecordsData.sale_records})
    
        }
    }


    render () {
        return (
            <div className="row">
                <div className="shadow p-4 mt-4">
                    <h1>Sales Person History</h1>
                    <form id="sales-person-history-form">
                        <div className="mb-3">
                            <select onChange={this.handleChange} value={this.state.sales_person} required id="sales_person" name="sales_person" className="form-select">
                                <option value="">All Sales</option>
                                {this.state.sales_people.map(sales_person => {
                                    return (
                                        <option key={sales_person.id} value={sales_person.id}>
                                            {sales_person.name}
                                        </option>
                                    );
                                })}
                            </select>
                            <label htmlFor="sales_person">Select a Sales Person</label>
                        </div>
                    </form>
                    <table className="table table-hover">
                        <thead>
                            <tr>
                                <th>Sales Person</th>
                                <th>Customer</th>
                                <th>Automobile VIN</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.sale_records.map(sale => {
                                let shown = ""
                                if (sale.sales_person.id.toString() !== this.state.sales_person){
                                    shown = "d-none"
                                }
                                return (
                                
                                <tr className={shown} key={sale.id}>
                                    <td>{sale.sales_person.name}</td>
                                    <td>{sale.customer.name}</td>
                                    <td>{sale.automobile.vin}</td>
                                    <td>${sale.sale_price}</td>
                                </tr>
                                )
                            })}
                        </tbody>
                    </table>
                    </div>
            </div>
        )
    }
}
export default SalesPersonHistory