import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ServiceList from './ServiceList';
import ServiceForm from './ServiceForm';
import ServiceHistory from './ServiceHistory';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import SaleRecordForm from './SaleRecordForm';
import SalesList from './SalesList';
import SalesPersonHistory from './SalesPersonHistory';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ModelList from './ModelList';
import ModelForm from './ModelForm';
import InventoryList from './InventoryList';
import AutomobileForm from './AutomobileForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="service">
            <Route path="" element={<ServiceList  />} />
            <Route path="new" element={<ServiceForm />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
          <Route path="sales">
            <Route path="new-salesperson" element={<SalesPersonForm />} />
            <Route path="new-customer" element={<CustomerForm />} />
            <Route path="new-sale" element={<SaleRecordForm />} />
            <Route path="" element={<SalesList />} />
            <Route path="salesperson-history" element={<SalesPersonHistory />} />
          </Route>
          <Route path="inventory">
            <Route path="" element={<InventoryList/>}/>
            <Route path="manufacturers" element={<ManufacturerList />} />
            <Route path="manufacturers/new" element={<ManufacturerForm/>}/>
            <Route path="models" element={<ModelList/>} />
            <Route path="models/new" element={<ModelForm />} />
            <Route path="automobiles/new" element={<AutomobileForm/>} />
          </Route>
          <Route path="technicians">
            <Route path="" element = {<TechnicianList />} />
            <Route path="new" element = {<TechnicianForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
