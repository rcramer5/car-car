from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json 
from common.json import ModelEncoder
from .models import AutomobileVO, Customer, SaleRecord, SalesPerson

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["color", "year", "vin", "import_href"]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number", "id"]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number", "id"]


class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = ["automobile", "sales_person", "sale_price", "customer", "id"]

    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_sale_records(request):
    if request.method == "GET":
        sale_records = SaleRecord.objects.all()
        return JsonResponse(
            {"sale_records": sale_records},
            encoder=SaleRecordEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Automobile href"},
                status=400,
            )
        try:
            salesperson_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=salesperson_id)
            content["sales_person"] = sales_person
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Sales Person Name" },
                status=400,
            )
        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer Name"},
                status=400,
            )
        sale_record = SaleRecord.objects.create(**content)
        return JsonResponse(
            sale_record,
            encoder=SaleRecordEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_create_sales_person(request):
    if request.method == "POST":
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )
    else:
        sale_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people": sale_people},
            encoder=SalesPersonEncoder,
        )

@require_http_methods(["GET", "POST"])
def api_create_customer(request):
    if request.method == "POST":
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )


@require_http_methods(["GET"])
def api_list_available_automobiles(request):
    sales = SaleRecord.objects.all()
    list_of_sold_vins = []
    for sale in sales:
        list_of_sold_vins.append(sale.automobile.vin)
    automobiles = list(filter(lambda x: x.vin not in list_of_sold_vins, AutomobileVO.objects.all()))
    return JsonResponse(
        {"available_autos": automobiles},
        encoder=AutomobileVOEncoder,
    )


