# CarCar

Team:

* Ryan - Services
* Christie - Sales

## Design
Our navigation bar is segmented into three containers, one for inventory, one for sales, and one for services. The services polled from the inventory to receive the automobiles and their corresponding VINs to check for VIP status. The sales microservice polled from inventory to receive the automobiles and the properties of each car. The inventory had to receive information from sales in order to show a real-time inventory of the automobiles based on whether the automobile had been listed in a sale or not. 

We decided to add a feature to our create a technician and create a sales person forms where it would update as you type in the employee numbers, it checks against the list of existing employee numbers to check for uniqueness, and informs the user if the employee number is already taken. It also checks for the correct length of an employee number, once again informing the user if their employee number is valid.
## Service microservice

Our service microservice polls from the inventory automobiles every 10 seconds and populates in an AutomobileVO model with the automobiles VIN and import href. We also have a technician model and a service appointment model. The technician model has name and employee number properties, and the service appointment has a foreign key to a technician, a VIN, an owner, a date/time, a reason for the visit, a completion boolean, and a VIP status boolean that checks to see if the VIN was ever in our inventory. 

We have a React page that has a search function to look up the service history of a specific VIN. The upcoming appointment React page has two buttons for each appointment; one to cancel the appointment, and one to complete the appointment. The cancel button deletes it from our database, and the complete button switches the completion property boolean to "true", effectively removing it from our upcoming appointments, and adding it to our service history. 

## Sales microservice

The sales microservice has four models. The sales person model has a name property and an employee number property. The customer model includes name, address, and phone number properties. There is an AutomobileVO model that polls from our inventory microservice every 10 seconds, populating with an automobile's vin, color, year, and import href. All of these models are important for our sale record model, which has foreign keys to the sales person, customer, and autmobileVO models. It also includes a sales price property. 

We included an available automobile view in our sales microservice that listed all of the automobiles from our inventory that had not yet been sold. To do this, we made a list of VINs from our sales records list and compared it to the list of VINs from our list of automobiiles that we polled from the inventory microservice. 

Our front end does everything that our api views does, but also allows a user to view our sales history per sales person. We included a dropdown of all of our sales people that, when chosen, populates a table of all of the sales they have made, including the customers name, the price, and the VIN of the automobile sold. 
